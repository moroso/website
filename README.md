# The Privacy Raccoon

We aim to help people defending their rights in the era of surveillance capitalism.
The site still needs a lot of work, especially a lot of content to be added. Hopefully
in the near future it will be a great resource for digital privacy.

Copying and contributing is encouraged!

# Contributing

**ALL CONTRIBUTIONS MUST BE UNDER THE CC0 LICENSE, IN ORDER TO KEEP
THESE RESOURCES IN THE PUBLIC DOMAIN AND AVAILABLE FOR EVERYONE**

When contributing we assume that you have agreed with your work being public domain.

Clone our [repository](https://codeberg.org/ThePrivacyRaccoon/website) and
create a pull request with your changes. If you become a usual contributor,
we will give you push privileges.


Thanks for contributing!

## Markup

You can write your guides or new pages in either [Markdown](https://en.wikipedia.org/wiki/Markdown)
or [HTML](https://en.wikipedia.org/wiki/HTML). Our static site generator support both of them,
just respect naming conventions: make sure that markdown files end with .md and that HTML files end with .html.

Unfortunately this has one limitation: you must edit existing pages in the markup language they are
written. Even if this might sound obvious, it's important to clarify.

## Generating the site

This isn't necessary for contributing, but maybe you want to check how your changes look locally
or you may want to setup a mirror (thanks!),
follow this instructions.

The site is generated with a very simple ssg written in POSIX shell, [ssg6](https://romanzolotarev.com/ssg.html).
Install it on your system with it's dependencies: lowdown, grep, find, cpio and a POSIX shell.
Then, clone our repo, create a destination directory and run a simple command:

~~~
mkdir dst
ssg6 raccoon dst "The Privacy Raccoon" "https://"
~~~

Done! The resulting site should be created in the dst directory. If you have
any issue, contact us.
