# The Madaidan

*There's much more to be said about this topic, this was extracted from a discussion
in the Spyware MUC*

One of the greatest problems in the community are a security researcher known as the Madaidan and
a GrapheneOS dev, Micay. They almost have the same ideas when it comes to security (and unfortunately privacy).
GrapheneOS devs attack everything with the excuse of security. Android ROMs are a security nightmare
because they don't have firmware updates and the bootloader is unlocked. Calyx is very insecure due to
signature sppofing. Firefox is quite insecure, so you must use Google's browsers. A known member of the
GrapheneOS matrix room has been attacking F-Droid and enhancing GrapheneOS store and the Aurora Store.

Micay, in the FAQ of GrapheneOS, says that the Linux kernel is insecure and that he's excited about
replacing it with a microkernel (Oh, did you know that Google's Fuchsia is a microkernel?). As they see 
it, we have to use Google hardware and software because every alternative is ridicously insecure.

They also recommend using smartphones (GrapheneOS in a Google device or non-jailbreaked iOS up to date)
over desktop computers because computers weren't design with security
in mind. And if you want to use a desktop, they recommend Windows 11 with secure boot. What a joke.
Windows is a major privacy offender. You can't be private in Windows. You may even get a hosts file with a ton
of blocked domains to block Microsoft's telemetry. But then, Windows will detect that behavior like a virus and
it deletes it.

Yeah, sure, a Google phone with proprietary bootloader and a proprietary TITAN M chip is the best option
for privacy.

Madaidan tends to recommend corporate software like Chromium, MacOS or Windows. He will shit on Linux and he completely
ignores OpenBSD, which has been awarded as an excellent security focused desktop OS. He recommends 
Signal, in spite of requiring Personal Identifiable Information like a phone number
and being centralized. He doesn't mention XMPP, which outclass Signal. In his browser article he doesn't even
recommend configuring your browser and he's against content blockers, saying that everyone configures their browser
differently and that you'll stand out. 

That is the average excuse to use Google Chrome. But there are other options: Moonchild, the main Pale Moon
dev takes the other approach, since he knows
that blending in is almost impossible, he makes his fingerprint unique in purpose, randomizing it with every page reload.

At the end, these recommendations are harmful for the user's privacy. If you want a truly secure boot, take a desktop
with support for Libreboot/Coreboot and make GRUB verify with GPG. That's a true secure, verified boot. Not like Microsoft's, which only
purpose is to force you into using Windows.

## Sources

* [The undercoverman](http://4ad3tmc4ljbunjek2zhg2flj3br6vmonxhsmi6kqfgiw6b2cx3inpuid.onion/posts/privacysites.xhtml)
* The Spyware MUC