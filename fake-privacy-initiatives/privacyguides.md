# PrivacyGuides

*UPDATE*: Avoid PrivacyGuides. They are recommending proprietary
software, such as Apple Mail and Canary Mail! ([archive.org](https://web.archive.org/web/20220824020934/https://www.privacyguides.org/email-clients/), [archive.is](https://archive.ph/QvmHU))
This is a **big red flag**. These programs are black boxes, we do not know what are they doing at all. The
user loses the control. **We, as privacy activists, should have high standards and do not allow recommending
software that violates users privacy**.


Created by a former maintainer of PrivacyTools.io. This site at least isn't Cloudflared. Let's see
if it's any better than PrivacyTools in their recommendations:

* **Cloudflare's DNS is [recommended](https://www.privacyguides.org/dns/)** ([archive.org](https://web.archive.org/web/20220824021919/https://www.privacyguides.org/dns/), [archive.is](https://archive.ph/3T1NY)).
* Firefox and Brave are recommended instead of Librewolf and Ungoogled Chromium.
* Additionally, they recommend Safari which is proprietary and spies on you.
* They recommend Thunderbird which continously phones home. It has a history of being vulnerable to major email attacks like [efail](https://efail.de/media/img/efail-disclosure-smime.png).
* Tutanota and Protonmail are in the email provider list.
* Fake privacy initiatives like Duckduckgo and Startpage are recommended.

Probably more, but this alone shows that there's no real privacy here. Yet another privacy website
which instead of digging deeper for real private alternatives they recommend
the mainstream solutions. They go as far as recommending proprietary spyware like Apple Mail or Safari.

This does not make PrivacyGuides useless, there are still good recommendations there. But **they 
recommend well-known privacy violators like Cloudflare** and proprietary software like Apple Mail and Canary Mail.
**This should not be tolerated** by privacy advocators. There are alternatives much more private that are just as easy to use.

What extra work does it cost the user to switch to a private DNS instead of switching to Cloudflare? If someone is changing their DNS servers
anyway, why do they recommend Cloudflare instead of a better provider? You have to follow exactly the same process
to change your DNS servers, no matter if it's Cloudflare's or Mullvad's. This applies to other recommendations they
make and we can't understand the reasoning behind such decisions.

*UPDATE2*: They are now recommending [1Password](https://www.privacyguides.org/passwords/#1password) ([archive.org](https://web.archive.org/web/20220905025212/https://www.privacyguides.org/passwords/#1Password), [archive.is](https://archive.ph/nCRgh)).
They give a bunch of reasons, yet we can't understand why are they recommending this. 1Password is a proprietary
password manager whose main feature is cloud sync (Bitwarden does exactly the same being free/libre software). Oh and
1Password is also paid, no free tier. This is definetely going to be a drawback for non technical users, which by reading
their arguments seem to be the target public for this recommendation. Bitwarden has an excellent free tier.
They mention that one advantage of 1Password over Bitwarden is excellent support for native clients. Bitwarden also has
native clients, but I don't think that matters because a non technical user will just use the web version anyway,
people does everything on their browser nowadays.

To sum up, we can't find a good reason for recommending the **proprietary 1Password** when you're already recommending
Bitwarden. The difference in terms of features is minimal, the dealbreaker is that Bitwarden is free/libre software while
1Password is proprietary.

## Sources

* [The undercoverman](http://4ad3tmc4ljbunjek2zhg2flj3br6vmonxhsmi6kqfgiw6b2cx3inpuid.onion/posts/privacysites.xhtml)
* [digdeeper](https://digdeeper.club/articles/fake_initiatives.xhtml#privacyguides)
