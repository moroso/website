# Mirrors

We encourage you to access our site through the darknet mirrors instead of the clearnet one. We would
appreciate if you could setup another mirror. They help avoiding censorship and archiving information.
[Contact us](/about.html#Contact) in case any mirror stop working.

## Darknet
   
* Tor: [gh7bylvohpjyni33ii3rd7ypyr6dqgjnwblm2hmamlmmqm2nxfx4iryd.onion](http://gh7bylvohpjyni33ii3rd7ypyr6dqgjnwblm2hmamlmmqm2nxfx4iryd.onion/)
* I2P: Coming soon.

## Clearnet

* Clearnet 1: [privacyraccoon.tk](https://privacyraccoon.tk)
* Clearnet 2: [raccoon.pinnoto.org](https://raccoon.pinnoto.org/)
