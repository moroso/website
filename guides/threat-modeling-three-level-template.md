# Threats modeling: The three-level template

It's a good idea to know your threat model before you start digging in the privacy and security rabbithole.
That way, you'll know which tools are overkill for you or which ones will be useful.

Threat modeling consists on deciding which level of security works better for you,
trying to balance security, privacy and convenience of use. Basically, threat modeling
means that you have to choose the level of security and privacy you want or need.

There are other approachs for threat modeling, and while they are useful I find them needlessly long.
Here we're simplifying things by using a three-level template. It's divided in three tiers.
Since you're on this website, I assume you care at least a little about privacy. That's why there is no
Tier 0.

Just take a look at the summary and choose the tier that fits you better. Then, scroll down and start taking
measures to improve your security and privacy right now!

* Tier 1 - Less protection, little learning required, almost no comfort loss
* Tier 2 - Greater protection, more learning required, some comfort loss
* Tier 3 - Highest protection, dedicated learning and comfort sacrifices required


**WARNING:** Note that this site is dedicated to **digital privacy**. Our digital self-defense guide can help
protecting yourself online. But
**it's out of the scope of this website to cover physical security**. If you want to cover your tracks in the
physical world too, read "Extreme Privacy: What it takes to Disappear" by Michael Bazzell. It's a fantastic book.

## Tier 1 - I want to take basic protection measures against mass surveillance and common cybercriminals.

The first threat model focuses on providing an enhanced level of privacy for the people who are not
willing to sacrifice some convenience.

This is where people who care about privacy but doesn't want to invert time in it fit. 
The convenience loss is insignificant. It's a kind learning curve
for the little difference between the tools you were using before and the new ones.

Some example actions are:

* Remove old accounts and reduce the overall number of useless accounts.
* Remove unnecessary programs and apps.
* Start using a [password manager](/guides/password-managers.html).
* Use Librewolf or Ungoogled Chromium instead of Chrome (Mull or Bromite for Android).
* Use a [privacy respecting search engine](/index.html#search-engine) instead of Google.
* Switch from Gmail/Outlook to a [private email provider](/index.html#email).
* Replace Windows with [Linux](/index.html#desktop-os). Remember to check Full disk encryption in the installer.
* Move most of their digital activity from the mobile phone to the laptop/desktop.
* Swap most programs with their free/libre alternatives (MS Office -> Libreoffice, Google Keep -> Joplin, etc).
* Use a recent Android version and do not vinculate it with a Google account.
* Use F-Droid instead of Google Play Store for most apps.
* Use [encrypted DNS](/index.html#dns-client). Might consider using a [VPN](/index.html#vpn).

*(example actions are not exhaustive lists. We might create dedicated articles in the future)*

## Tier 2 - I want enhanced protection against mass surveillance and targeted cybercriminals.

The second tier is where a lot of tech literate people will want to be. Be a ghost in the internet.
It also fits people who want or need free of speech, like scientists and activists (not actively prosecuted).

This level requires be willing to learn how to use new tools and good practices to remain private.
Some convenience may be lost because there's some learning curve, especially if you have no IT knowledge.

Some example actions are:

* Reduce your presence online as much as possible for your person (some may need a public portfolio, work related sites).
* Remove every social media (you may join [the fediverse](/index.html#social) under an alias)
* Separate your work and personal identity online.
* Encrypt your mails using PGP.
* Start using XMPP with OMEMO instead of other centralized instant messengers like Whatsapp, Telegram, Signal, etc.
* Use [private frontends](/index.html#private-frontends) instead of visiting privacy invasive websites like YouTube or Twitter.
* Use only free/libre software.
* Make extensive use of Tor to browse the web, a VPN may be required for sites that block Tor.
* Disable JavaScript in your web browser.
* Reduce web browser usage, move to native programs when possible.
* If you need to use a new service, try using it exclusively through Tor, with an unique username and email alias.
* Harden your Linux desktop with security in mind. Use intrusion detection techniques.
* Use a [custom Android ROM](/index.html#mobile-os).
* Avoid "smart" products (smartwatches, smart TVs, etc).

Tip: scroll down through our [Recommendations](/index.html). It's a complete catalogue of
privacy respecting programs and services. I'm sure that you'll find some programs useful.

## Tier 3 - I want to protect my online presence from eavesdropping of my local and foreign alphabet agencies/governments.

This third tier is meant for people in dangerous situations, like journalists and activists under
opressive governments.

Please, understand that there are high security measures, but you will never achieve full security. There is nothing
invulnerable.

Example actions (for your online presence, if you want real world privacy/security take a look at the warning above):

* Run away from anything "smart".
* Limit your use of the internet.
* Ideally you would not use the internet. But we understand that it's essential for journalism and activism.
* You make an extensive use of aliases, ideally one for each thing, remove and create new ones frequently.
* Connect to the internet solely through Tor using a hardened Operating System.
* The best OS to use is [Qubes OS](/index.html#desktop-os) + [Whonix](/index.html#anon-os).
* For short computer sessions and web browsing, use [Tails OS](/index.html#anon-os).
* Get a Corebooted/Librebooted computer if possible. Make sure to disable [Intel ME](https://en.wikipedia.org/wiki/Intel_ME).
* Avoid mobile phones. 
* If you truly need a phone, never insert a SIM card, keep it WiFi only. Do not enable Bluetooth or NFC, never. Use a hardened and degoogled Android custom ROM (FDE, of course). Connect it to the internet solely through Tor, no exceptions. Keep it updated. No vital information should be in the phone. Use free/libre apps exclusively. Keep it on only when you are using it. The rest of the time it should be off and inside of a Faraday bag.
* The good way to do communications is XMPP + OMEMO used over Tor. Try to avoid email as much as possible. If needed, you should be using throwaway accounts on [good email providers](/index.html#email) encrypting them with PGP, using a native [email client](/index.html#email-clients) and connecting exclusively through Tor. Constantly rotate both XMPP and email accounts, maybe keep a main one for close people, but you must be careful.
* **This is not an exhaustive list. Seek professional help**

In the future, I'd like to create a dedicated guide about this threat model, since there is very little content in the internet about it.

# Which threat model does The Privacy Raccoon focus on?

We're privacy activists who hate how the internet has been ruined by [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism).
Because we actively fight against surveillance, **we do our best to stick to the maximum level of privacy and security**.

This means that our recommendations are mostly for the second tier and the surface of the third tier.

Remember that there is no silver bullet when it comes to privacy. The ultimate privacy/security tool does not exist.
However, various useful tools combined, used properly and following good practices can result in very good results.
Plan your threat model accordingly to your personal needs, not everyone needs the highest level of privacy.
