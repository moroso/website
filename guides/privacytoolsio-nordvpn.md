# PrivacyToolsIO failing to their principles: dark patterns and the hidden contract with NordVPN.
#### 12/08/22

> PrivacyTools.io

> The most reliable source for privacy tools since 2015. Software and services to counter surveillance with encryption for better internet privacy.

This is PrivacyToolsIO bio in Twitter. This used to be true, but that's no more the case.
PrivacyTools has losen every bit of credibility. I'm writing this article to bring focus on what's happening with
this website.

To begin with, the first thing I noticed when visiting PrivacyTools.io today was that there was a new
link in their header, on which it says "VPN". Next to it there was another link, "Mail".
My first thought was that they were offering their own VPN and email community services, like they used to
offer other free services in the past, maintained with donations money.
That seemed to be great. I clicked on the "VPN" link and to my surprise, it was a link to NordVPN.
I was kinda confused, because even if PrivacyToolsIO didn't have the best privacy recommendations,
I knew that they have standards and NordVPN was way below them. This was the first red flag.

Then, I clicked on email. It redirected me to Protonmail. I was surprised though, because they
weren't affiliated links. Or that's what I saw using my extremely hardened Firefox. I spun up a VM,
launched the Tor browser and visited their website again. These were the links I was redirected to:

* https://nordvpn.com/special/?utm_medium=affiliate&utm_term&utm_content&utm_campaign=off15&utm_source=aff74417
* https://proton.me/mail?url_id=267&utm_campaign=ww-all-2c-mail-gro_aff-g_acq-partners_program&utm_source=aid-tune-3280&utm_medium=link&utm_term=generic_mail_landing&utm_content=26

These are indeed affiliated links.

I was intrigued to see if there were more changes to the site. I didn't need to scroll down too much,
the first section on the website, which used to be web browsers since 2015, had been moved to the second
position. Now, the first section is VPNs and at the top of the VPNs list, being the very first recommendation
on the whole site, NordVPN. ([archive 1](https://web.archive.org/web/20220811012717/https://www.privacytools.io/), [archive 2](https://archive.ph/Xnm3c))

I scrolled through the whole site and I noticed yet another substantial change: In the password manager section,
there was a new top recommendation: NordPass, a password manager brought to you by NordVPN. It was also linked
using an affiliated link ([archive](https://web.archive.org/web/20220811012717/https://www.privacytools.io/#password)).

One of their catch phrases at the bottom of the site is "Using Open-Source Software for Your Security".
But this proprietary password manager was being listed over trusted free/libre software like KeePassXC and Bitwarden, both of
them having an excellent reputation in security circles. Those used to be the main recommendations
in PrivacyToolsIO in the past. But now they are below a password manager of doubtful trust.

There was yet another NordVPN affiliated link on the site: NordLocker, a cloud storage service. Following NordVPN's guidelines,
NordLocker is also proprietary and still listed over the obvious pro-privacy choice, Nextcloud ([archive](https://web.archive.org/web/20220811012717/https://www.privacytools.io/#cloud)).


About Protonmail, their ProtonVPN appears as an affiliated link just below NordVPN, their calendar is the top
recommendation in the calendar section and of course it's the top mail provider. Everything under an affiliated link.

If you thought that this was shady, wait. Because this is the point where things start getting worse.
I decided to go to their Github repo and check when this NordVPN non-sense got added.
But their links to Github had vanished. 

I found their [repo on Github](https://github.com/privacytoolsIO/privacytools.io)
but **their website's source was gone**. All that remained was the README, which only says is "Suggestions and Discussions for PrivacyTools.io".
(I know it's the correct repository because in their profile they have a repository for their "Github
Gems", where they link to their new forum).

Okay, so no more source for the website. They do have a link for their changelog. I'd use that to see the date of the changes.
In the [changelog](https://forum.awesomealternatives.org/t/privacytools-changelog) ([archive 12/08/22](https://archive.ph/IskvA))
they mentioned every new recommendation added or removed, **but they did not mention NordVPN nor NordPass**.

I thought that they should have announced it on their [blog](https://www.privacytools.io/guides/) and that's why it didn't
appeared on the changelog. Once again, nothing. Not a single mention to the recent changes. Then I checked their 
[forums](https://forum.awesomealternatives.org) but there was nothing there.

In a desperate attempt to give them another opportunity, I went to their Twitter profile and scrolled through every tweet of
the last 3 months. There was no announcement of a partnership with NordVPN.

The lack of transparency is worrying. Quoting their website "Transparency reports and warrant canaries have an important
role to play in the fight against illegal and unconstitutional national security process". But their behaviour has been
opaque lately.

Since they didn't want to tell us when they added this, I went to the WaybackMachine and checked when this was added myself

After some trial and error, I've discovered that the change was made exactly on August 01:

* [29/07/2022 - No VPN affiliated link, browsers were the first section](https://web.archive.org/web/20220729073033/https://www.privacytools.io/),
* [01/08/2022 - VPN affiliated link, VPNs are the first section](https://web.archive.org/web/20220801103129/https://www.privacytools.io/)

It is clear from these observations that **there is a commercial contract between PrivacyToolsIO and NordVPN**. (and potentially another one
with Protonmail). This means they are failing to their principles, because PrivacyToolsIO assured to not include affiliates. Check the website's
footer in this [archive](https://web.archive.org/web/20150416013551/https://www.privacytools.io/) if you don't believe me.
They didn't include affiliated links because they know that they can make their recommendations biased due to
monetary interests. That's exactly what have happened.

**They've prioritized money over their readers' privacy** by recommending proprietary services which are
harmful for your privacy (If you think that NordVPN is any good, read below).

I'm honestly sad to see how PrivacyTools.io's website content quality and standards has degraded, because back in 2016 this website used to be my
one of my favorite places on the internet. **But this only reaffirms me in my efforts to create a site with truly privacy tools at
[The Privacy Raccoon](/index.html)**

***UPDATE***: I missed this paragraph at the bottom of the [donation page](https://www.privacytools.io/donate/) ([archive](https://archive.ph/oozaf)).
Read carefully:

> Affiliate Programs

> I will make this very clear: No tool or service will be added here just because an affiliate program is offered. Only existing entries are affected, clearly marked and are designed for commercial services that gained an entry already on PrivacyTools.io independently from the existance of an affiliate program. This method will ensure that the quality of recommendations on PrivacyTools.io wont be affected. 

This is a big lie. Check this [archive](https://web.archive.org/web/20220726194116/https://www.privacytools.io/#vpn) from 26/07/2022. **NordVPN
was not recommended in their VPN section**. We do not see NordPass and NordLocker either. **NordVPN was first listed on [27/07/2022](https://web.archive.org/web/20220728123233/https://www.privacytools.io/#vpn)**
without the header sponsored link and their other products are missing. But still, we can verify that **it was added directly as an affiliated program**,
in contrast to their Affiliate Programs policy. **NordPass, NordLocker were not recommended before being affiliated either**.
They were added together with the sponsored link on August ([29/07/22 archive](https://web.archive.org/web/20220729073033/https://www.privacytools.io/)), when we
believe that the contract started.

They are blatantly lying. I'm glad that I've archived almost every single source in this article, because I'm sure that they will
do everything they can in order to hide this.

## The problem with NordVPN

Why has PrivacyTools betrayed all their readers privacy with the inclusion of NordVPN? NordVPN claims to
store no logs and it's based on Panama, out of the 15-eyes countries.

Well, the lesson here is that **you shouldn't believe in marketing strategies, but in actual facts**. Let me show
you a few things:

I didn't mention it because I think that it's well-known, but NordVPN is at the very least **controversial**
in terms of privacy. 
In an interview, NordVPN recognized to use the following third party services: "Google Analytics to monitor website
and app data, as well as Crashlytics, Firebase Analytics and Appsflyer to monitor application data." 
([source](https://torrentfreak.com/vpn-review-anonymous-nordvpn/), [archive](https://archive.ph/Uz0TG)). They track their clients
using well-known **third party violators like Google Analytics and Firebase**. NordVPN's apps are proprietary, of course.

Do you know about [Terms of Service didn't read](https://www.tosdr.org/)? It's a great website where they take companies
ToS, classify them and create a human readable format. Let's take a look at [NordVPN's](https://www.tosdr.org/en/service/697):

* Logs are kept for an undefined period of time
* Terms may be changed any time at their discretion, without notice to the user
* This service ignores the Do Not Track (DNT) header and tracks users anyway even if they set this header.
* This service may use your personal information for marketing purposes
* The service can sell or otherwise transfer your personal data as part of a bankruptcy proceeding or other type of financial transaction.
* Instead of asking directly, this Service will assume your consent merely from your usage.
* The service may use tracking pixels, web beacons, browser fingerprinting, and/or device fingerprinting on users.
* This service gives your personal data to third parties involved in its operation
* Your data may be processed and stored anywhere in the world.
* The service is only available in some countries approved by its government.
* This service uses third-party cookies for statistics.
* Tracking pixels are used in service-to-user communication.

Not only that, because while reading their [Terms of Service](https://my.nordaccount.com/legal/terms-of-service/) I've found something interesting:

> 8.4 We use automated tools to identify web scraping and minimize abuse of our Services. These tools are looking for irregular patterns when new sessions are initiated and if such patterns are noticed, it might automatically suspend your Account or otherwise limit your access to the Services until further investigation is complete.
([archive, section 8.4](https://archive.ph/5qQ4G))

This section was recently added, since I've read this [archive](https://archive.ph/kv8P9) from 9 Apr 2022 and the section 8 was completely different.
This means that they are continuously scanning your traffic.

Remember that a key part of NordVPN's marketing is arguing that thanks to Panama's laws they are able to protect your privacy?
I've happened to find out that this is not completely true. This [article](https://www.technadu.com/who-owns-nordvpn/295187/)
tells us that NordVPN is currently owned by [Nord Security](https://nordsecurity.com/) (NordSec Ltd.). 

> And if we check NordVPN’s US registration number, we can see that the current owner is NordSec Ltd. However, what’s interesting to note is that NordSec is registered in London, the UK. 

Oh, so NordVPN's parent company is registered in the United Kingdom, one of the co-founders of the infamous 5-eyes.
But on NordVPN's website there is not a single mention of this. They will always tell you that they're Panama based.
That's a half-truth. NordVPN's is a legal mess:

> Well, as per NordVPN’s (highly complex) legal documents, Panama is where NordVPN was incorporated, Cyprus is where NordVPN’s attorneys and registered directors are located, and the UK is where NordSec's offices are found. Lastly, Lithuania is where NordVPN’s team comes from and where most of its employees work on a daily basis. Complicated, right?

I didn't want to rely on a single source, so I've found more information about Nord Security. The site Craft.co
tells us [their locations](https://craft.co/nord-security/locations) ([archive](https://archive.ph/CSYij)):

* United Kingdom	London	4 Old Park Lane, Mayfair, London (HeadQuarters)
* Germany	Berlin, Germany
* Lithuania	Kaunas, Lithuania
* Lithuania	Vilnius, Lithuania

For people who will still defend NordVPN, Craft.co is not reviewing a random VPN service, they are a website that provides services 
for businesses that want to build
strong suppliers networks, so it has the data of tons of companies around the world. It happened to have Nord Security location data.

What is the conclusion then? Although NordVPN is based in Panama, NordVPN is registered in the US in order to operate in
the country, which means that it should comply with their laws. And the parent company is based in London, UK.
Given their ToS and that they incorporate analytics and trackers on their proprietary apps, they do not prove that they
care about their customers privacy, but just the opposite.

PrivacyToolsIO is aware of NordVPN's privacy flaws, because NordVPN was not recommended until their commercial agreement. And there have been various
posts on their subreddit (now dead) about NordVPN questionable privacy 
([post 1 - teddit](https://teddit.net/r/privacytoolsIO/comments/bhxbyz/theres_nordvpn_odd_about_this_right_infosec_types/),
[post 1 - reddit](https://old.reddit.com/r/privacytoolsIO/comments/bhxbyz/theres_nordvpn_odd_about_this_right_infosec_types/),
[post 2 - teddit](https://teddit.net/r/privacytoolsIO/comments/9b69eo/nordvpn_official_statement_for_allegation/),
[post 2 - reddit](https://old.reddit.com/r/privacytoolsIO/comments/9b69eo/nordvpn_official_statement_for_allegation/))

All of this points to the conclusion that **the only reason NordVPN has been added to PrivacyToolsIO is monetary**. And
they've done that at the expense of their users' privacy, listing their proprietary services over free/libre alternatives
which are well known and battle-tested.

##### I want to thank Stella from the Spyware MUC for helping me finding sources about Nord Security
